module.exports = function(grunt) {
	grunt.loadNpmTasks('grunt-loopback-sdk-angular');
	grunt.loadNpmTasks('grunt-docular');

	grunt.initConfig({
		loopback_sdk_angular: {
			options: {
				input: 'server/server.js',
				output: 'client/js/ballet-services.js',        // Other task-specific options go here.
				ngModuleName: 'ngBalletService'
			},
			staging: {
				options: {
					apiUrl: 'http://api-vader.rhcloud.com/api'
				}
			},
			production: {
				options: {
					apiUrl: 'http://api-vader.rhcloud.com/api'
				}
			}
		},
		docular: {
			groups: [
				{
					groupTitle: 'LoopBack',
					groupId: 'loopback',
					sections: [
						{
							id: 'ngBalletService',
							title: 'LoopBack Services',
							scripts: [ 'js/ballet-services.js' ]
						}
					]
				}
			]
		}
	});

	grunt.registerTask('default', ['loopback_sdk_angular', 'docular']);
};
